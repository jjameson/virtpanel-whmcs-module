<?php
/**
 * VirtPanel WHMCS Module v3.1
 *
 * @see https://www.virtpanel.com/User/Documentation/View/19
 *
 * @copyright Copyright (c) VirtPanel 2016
 * @license https://www.virtpanel.com/Licensing+Agreement VirtPanel Licensing Agreement
 */

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Define module related meta data.
 *
 * Values returned here are used to determine module related abilities and
 * settings.
 *
 * @see http://docs.whmcs.com/Provisioning_Module_Meta_Data_Parameters
 *
 * @return array
 */
function virtpanel_MetaData()
{
    return array(
        'DisplayName' => 'VirtPanel',
        'APIVersion' => '1.1',
        'RequiresServer' => true,
        'DefaultNonSSLPort' => '80',
        'DefaultSSLPort' => '443',
        'ServiceSingleSignOnLabel' => 'Login to VirtPanel as User',
        'AdminSingleSignOnLabel' => 'Login to VirtPanel as Admin',
    );
}

/**
 * Define product configuration options.
 *
 * The values you return here define the configuration options that are
 * presented to a user when configuring a product for use with the module. These
 * values are then made available in all module function calls with the key name
 * configoptionX - with X being the index number of the field from 1 to 24.
 *
 * You can specify up to 24 parameters, with field types:
 * * text
 * * password
 * * yesno
 * * dropdown
 * * radio
 * * textarea
 *
 * Examples of each and their possible configuration parameters are provided in
 * this sample function.
 *
 * @return array
 */
function virtpanel_ConfigOptions()
{
    return array(
		// configoption1
		'Virtualization Type' => array(
            'Type' => 'dropdown',
            'Options' => array(
                'option1' => 'Qemu',
            ),
            'Description' => 'Choose virtualization',
        ),
		
		// configoption2
		'Server Pool' => array(
            'Type' => 'text',
            'Size' => '40',
            'Default' => '',
            'Description' => 'Which server pool to create the VPS on? Leave blank for any pool',
        ),
		
		// configoption3
        'Resource Plan' => array(
            'Type' => 'text',
            'Size' => '40',
            'Default' => '-custom-',
            'Description' => 'Name of the resource plan. If using configurable options set this to: <b>-custom-</b>',
        ),
		
		// configoption4
		'IPv4 Addresses' => array(
            'Type' => 'text',
            'Size' => '5',
            'Default' => '1',
            'Description' => 'Number of IPv4 addreses to assign',
        ),
		
		// configoption5
		'IPv6 Blocks' => array(
            'Type' => 'text',
            'Size' => '5',
            'Default' => '1',
            'Description' => 'Number of IPv6 blocks to allocate',
        ),
		
		// configoption6
		'Template' => array(
            'Type' => 'text',
            'Size' => '40',
            'Default' => '',
            'Description' => 'Enter the template filename to install. Leave blank to allow your client to install an OS from a list after setup',
        ),
    );
}

/**
 * Provision a new instance of a product/service.
 *
 * Attempt to provision a new instance of a given product/service. This is
 * called any time provisioning is requested inside of WHMCS. Depending upon the
 * configuration, this can be any of:
 * * When a new order is placed
 * * When an invoice for a new order is paid
 * * Upon manual request by an admin user
 *
 * @param array $params common module parameters
 *
 * @see http://docs.whmcs.com/Provisioning_Module_SDK_Parameters
 *
 * @return string "success" or an error message
 */
function virtpanel_CreateAccount(array $params)
{
    try {
		
		// debug
		//return var_export($params, true);
		
		$type = strtolower($params['configoption1']);
		$pool = $params['configoption2'];
		$plan = $params['configoption3'];
		$ipv4 = $params['configoption4'];
		$ipv6 = $params['configoption5'];
		$template = $params['configoption6'];
		
		if (empty($pool)) {
			$pool = '(Any Pool)';
		}
		
		// Generate a username if blank
		$params['username'] = trim($params['username']);
		if (empty($params['username'])) {
			$firstname = strtolower($params['clientsdetails']['firstname']);
			$lastname = strtolower($params['clientsdetails']['lastname']);
			$allowedchars = range('a', 'z');
			
			for ($i=0;$i<strlen($firstname);$i++) {
				if (in_array($firstname[$i], $allowedchars)) {
					$params['username'] .= $firstname[$i];
					break;
				}
			}
			for ($i=0;$i<strlen($lastname);$i++) {
				if (in_array($lastname[$i], $allowedchars)) {
					$params['username'] .= $lastname[$i];
					break;
				}
			}

			$params['username'] .= $params['serviceid'];
		}
		
		$params['password'] = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		
		$domain = $params['domain'];
		if (empty($domain)) {
			$domain = $params['customfields']['Hostname'];
		}
		
		// Update the username & password
		try {
			$updatedUserCount = Capsule::table('tblhosting')
				->where('id', $params['serviceid'])
				->update(
					[
						'username' => $params['username'],
						'password' => encrypt($params['password']),
						'domain' => $domain,
					]
				);
		} catch (\Exception $e) {
			return "Failed to update service username in the database. {$e->getMessage()}";
		}
		
		$post = array(
			'action' => 'create',
			'type' => $type,
			'username' => $params['username'],
			'password' => $params['password'],
			'email' => $params['clientsdetails']['email'],
			'hostname' => $domain,
			'ip_addresses_from_pool' => $ipv4,
			'ipv6' => $ipv6,
			'server' => '(Automatic)',
			'server_pool' => $pool,
			'create' => 'create',
			'continue' => 'continue',
		);
		
		$post['plan'] = $plan;
		if ($plan=='-custom-') {
			// Use configurable options for custom limits
			foreach($params['configoptions'] as $k => $v) {
				$post[$k] = $v;
			}
		}
				
		$data = virtpanel_curl($params, '?vm', $post);
		if ($data===false) {
			return virtpanel_return_errors();
		}
		if (stripos($data, 'CREATED_SUCCESSFULLY')!==false) {
			return virtpanel_get_ip($params);
		}
		return $data;
		
    } catch (Exception $e) {
        // Record the error in WHMCS's module log.
        logModuleCall(
            'provisioningmodule',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return 'success';
}

/**
 * Suspend an instance of a product/service.
 *
 * Called when a suspension is requested. This is invoked automatically by WHMCS
 * when a product becomes overdue on payment or can be called manually by admin
 * user.
 *
 * @param array $params common module parameters
 *
 * @see http://docs.whmcs.com/Provisioning_Module_SDK_Parameters
 *
 * @return string "success" or an error message
 */
function virtpanel_SuspendAccount(array $params)
{
    try {
        
		$data = virtpanel_curl($params, '?vm', array(
			'action' => 'view',
			'vm' => $params['username'],
			'subaction' => 'suspend',
			'full' => 'true',
		));
		if ($data===false) {
			if ($GLOBALS['virtpanel_module_errors'][0]=='Account does not exist') {
				return "success";
			}
			return virtpanel_return_errors();
		}
		if (stripos($data, 'successfully updated')!==false) {
			return 'success';
		}
		return $data;
		return 'Unknown Error';
		
    } catch (Exception $e) {
        // Record the error in WHMCS's module log.
        logModuleCall(
            'provisioningmodule',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return 'success';
}

/**
 * Un-suspend instance of a product/service.
 *
 * Called when an un-suspension is requested. This is invoked
 * automatically upon payment of an overdue invoice for a product, or
 * can be called manually by admin user.
 *
 * @param array $params common module parameters
 *
 * @see http://docs.whmcs.com/Provisioning_Module_SDK_Parameters
 *
 * @return string "success" or an error message
 */
function virtpanel_UnsuspendAccount(array $params)
{
    try {
        
		$data = virtpanel_curl($params, '?vm', array(
			'action' => 'view',
			'vm' => $params['username'],
			'subaction' => 'unsuspend',
			'full' => 'true',
		));
		if ($data===false) {
			return virtpanel_return_errors();
		}
		if (stripos($data, 'success')!==false || stripos($data, 'Account does not exist')!==false) {
			return "success";
		}
		return 'Unknown Error';
		
    } catch (Exception $e) {
        // Record the error in WHMCS's module log.
        logModuleCall(
            'provisioningmodule',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return 'success';
}

/**
 * Terminate instance of a product/service.
 *
 * Called when a termination is requested. This can be invoked automatically for
 * overdue products if enabled, or requested manually by an admin user.
 *
 * @param array $params common module parameters
 *
 * @see http://docs.whmcs.com/Provisioning_Module_SDK_Parameters
 *
 * @return string "success" or an error message
 */
function virtpanel_TerminateAccount(array $params)
{
    try {
        
		virtpanel_get_ip($params);
		$data = virtpanel_curl($params, '?vm&vm['.urlencode($params["username"]).']='.urlencode($params["username"]), array(
			'delete' => 'Delete',
		));
		if ($data===false) {
			if ($GLOBALS['virtpanel_module_errors'][0]=='Account does not exist') {
				return "success";
			}
			return virtpanel_return_errors();
		}
		if (stripos($data, 'successfully deleted')!==false) {
			return "success";
		}
		return 'Unknown Error';
		
    } catch (Exception $e) {
        // Record the error in WHMCS's module log.
        logModuleCall(
            'provisioningmodule',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return 'success';
}

/**
 * Change the password for an instance of a product/service.
 *
 * Called when a password change is requested. This can occur either due to a
 * client requesting it via the client area or an admin requesting it from the
 * admin side.
 *
 * This option is only available to client end users when the product is in an
 * active status.
 *
 * @param array $params common module parameters
 *
 * @see http://docs.whmcs.com/Provisioning_Module_SDK_Parameters
 *
 * @return string "success" or an error message
 */
function virtpanel_ChangePassword(array $params)
{
    try {
		
		$data = virtpanel_curl($params, '?vm', array(
			'action' => 'view',
			'vm' => $params['username'],
			'subaction' => 'pass',
			'new' => $params['password'],
			'new2' => $params['password'],
			'update' => 'update',
		));
		if ($data===false) {
			return virtpanel_return_errors();
		}
		if (stripos($data, 'successfully updated')!==false) {
			return "success";
		}
		return 'Unknown Error';
		
    } catch (Exception $e) {
        // Record the error in WHMCS's module log.
        logModuleCall(
            'provisioningmodule',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return 'success';
}

/**
 * Upgrade or downgrade an instance of a product/service.
 *
 * Called to apply any change in product assignment or parameters. It
 * is called to provision upgrade or downgrade orders, as well as being
 * able to be invoked manually by an admin user.
 *
 * This same function is called for upgrades and downgrades of both
 * products and configurable options.
 *
 * @param array $params common module parameters
 *
 * @see http://docs.whmcs.com/Provisioning_Module_SDK_Parameters
 *
 * @return string "success" or an error message
 */
function virtpanel_ChangePackage(array $params)
{
    try {
	
		$plan = $params['configoption3'];
        
		if ($plan=='-custom-') {
			// Change Limits based on Configurable Options
			$post = array(
				'action' => 'view',
				'vm' => $params['username'],
				'subaction' => 'limits',
				'continue' => 'continue',
			);
			foreach($params['configoptions'] as $k => $v) {
				$post[$k] = $v;
			}
			$data = virtpanel_curl($params, '?vm', $post);
			if ($data===false) {
				return virtpanel_return_errors();
			}
			if (stripos($data, 'successfully updated')!==false) {
				return "success";
			}
			return 'Unknown Error';
		} else {
			// Change Plan
			$data = virtpanel_curl($params, '?vm', array(
				'action' => 'view',
				'vm' => $params['username'],
				'subaction' => 'plan',
				'plan' => $plan,
				'update' => 'update',
			));
			if ($data===false) {
				return virtpanel_return_errors();
			}
			if (stripos($data, 'successfully updated')!==false) {
				return "success";
			}
			return 'Unknown Error';
		}
	
    } catch (Exception $e) {
        // Record the error in WHMCS's module log.
        logModuleCall(
            'provisioningmodule',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return 'success';
}

/**
 * Test connection with the given server parameters.
 *
 * Allows an admin user to verify that an API connection can be
 * successfully made with the given configuration parameters for a
 * server.
 *
 * When defined in a module, a Test Connection button will appear
 * alongside the Server Type dropdown when adding or editing an
 * existing server.
 *
 * @param array $params common module parameters
 *
 * @see http://docs.whmcs.com/Provisioning_Module_SDK_Parameters
 *
 * @return array
 */
function virtpanel_TestConnection(array $params)
{
    try {
        
		$result = virtpanel_AdminSingleSignOn($params);

		if ($result['success']===true) {
			$success = true;
			$errorMsg = '';
		} else {
			$success = false;
			$errorMsg = 'Communcation Error';
		}
        
    } catch (Exception $e) {
        // Record the error in WHMCS's module log.
        logModuleCall(
            'provisioningmodule',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        $success = false;
        $errorMsg = $e->getMessage();
    }

    return array(
        'success' => $success,
        'error' => $errorMsg,
    );
}

/**
 * Additional actions an admin user can invoke.
 *
 * Define additional actions that an admin user can perform for an
 * instance of a product/service.
 *
 * @see virtpanel_buttonOneFunction()
 *
 * @return array
 */
function virtpanel_AdminCustomButtonArray()
{
    return array(
        "Power Off &amp; Start" => "reboot",
		"Update WHMCS IPs" => "get_ip",
    );
}

/**
 * Additional actions a client user can invoke.
 *
 * Define additional actions a client user can perform for an instance of a
 * product/service.
 *
 * Any actions you define here will be automatically displayed in the available
 * list of actions within the client area.
 *
 * @return array
 */
function virtpanel_ClientAreaCustomButtonArray()
{
    return array(
		"Login to VirtPanel" => "user_login",
        "Power Off &amp; Start" => "reboot",
    );
}

/**
 * Admin services tab additional fields.
 *
 * Define additional rows and fields to be displayed in the admin area service
 * information and management page within the clients profile.
 *
 * Supports an unlimited number of additional field labels and content of any
 * type to output.
 *
 * @param array $params common module parameters
 *
 * @see http://docs.whmcs.com/Provisioning_Module_SDK_Parameters
 * @see virtpanel_AdminServicesTabFieldsSave()
 *
 * @return array
 */
function virtpanel_AdminServicesTabFields(array $params)
{
    try {
	
		$vm = virtpanel_get_vm($params);
		
		if (empty($vm)) {
			return array();
		}
	
		switch($vm['status']) {
			case 'running':
				$status = '<span class="label label-success">Running</span>';
				break;
			case 'stopped':
				$status = '<span class="label label-info">Stopped</span>';
				break;
			case 'created':
				$status = '<span class="label label-info">Awaiting template selection</span>';
				break;
			case 'task':
				$status = '<span class="label label-info">Task in progress</span>';
				break;
			default:
				$status = ucwords($vm['status']);
				break;
		}
        
        return array(
            'Power Status' => $status,
            'IP Addresses' => $vm['ipaddresses'],
			'Server' => $vm['server'],
			'Template' => (empty($vm['template'])?'<span class="label label-danger">Not Installed</span>':$vm['template']),
			'Bandwidth Downloaded' => number_format($vm['bandwidth_i']/1024/1024/1024, 2) .' GB',
			'Bandwidth Uploaded' => number_format($vm['bandwidth_o']/1024/1024/1024, 2) .' GB',
        );
		
    } catch (Exception $e) {
        // Record the error in WHMCS's module log.
        logModuleCall(
            'provisioningmodule',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        // In an error condition, simply return no additional fields to display.
    }

    return array();
}

/**
 * Perform single sign-on for a given instance of a product/service.
 *
 * Called when single sign-on is requested for an instance of a product/service.
 *
 * When successful, returns a URL to which the user should be redirected.
 *
 * @param array $params common module parameters
 *
 * @see http://docs.whmcs.com/Provisioning_Module_SDK_Parameters
 *
 * @return array
 */
function virtpanel_ServiceSingleSignOn(array $params)
{
    try {
	
		$auth = virtpanel_curl($params, 'api/login_auth.php', array(
			'type' => 'vm',
			'username' => $params['username'],
		));
		$host = $params['serverhostname'];
		
		if (strlen($auth)==64) {
			return array(
				'success' => true,
				'redirectTo' => 'http'.($params['serversecure']=='on'?'s':'').'://'.$host.'/?login&username='.urlencode($params['username']).'&auth='.urlencode($auth),
			);
		} else {
			return array(
				'success' => false,
				'errorMsg' => 'Failed to get login auth key from VirtPanel',
			);
		}

    } catch (Exception $e) {
        // Record the error in WHMCS's module log.
        logModuleCall(
            'provisioningmodule',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return array(
            'success' => false,
            'errorMsg' => $e->getMessage(),
        );
    }
}

/**
 * Perform single sign-on for a server.
 *
 * Called when single sign-on is requested for a server assigned to the module.
 *
 * This differs from ServiceSingleSignOn in that it relates to a server
 * instance within the admin area, as opposed to a single client instance of a
 * product/service.
 *
 * When successful, returns a URL to which the user should be redirected to.
 *
 * @param array $params common module parameters
 *
 * @see http://docs.whmcs.com/Provisioning_Module_SDK_Parameters
 *
 * @return array
 */
function virtpanel_AdminSingleSignOn(array $params)
{
    try {
        
		$auth = virtpanel_curl($params, 'api/login_auth.php', array(
			'type' => 'users',
			'username' => $params['serverusername'],
		));
		$host = $params['serverhostname'];
		
		if (strlen($auth)==64) {
			return array(
				'success' => true,
				'redirectTo' => 'http'.($params['serversecure']=='on'?'s':'').'://'.$host.'/?login&username='.urlencode($params['serverusername']).'&auth='.urlencode($auth),
			);
		} else {
			return array(
				'success' => false,
				'errorMsg' => 'Failed to get login auth key from VirtPanel',
			);
		}
		
    } catch (Exception $e) {
        // Record the error in WHMCS's module log.
        logModuleCall(
            'provisioningmodule',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return array(
            'success' => false,
            'errorMsg' => $e->getMessage(),
        );
    }
}

/*
 * Custom VirtPanel Functions below
 */

function virtpanel_curl($params, $action, $vars = array()) {

	// debug
	//return var_export($params, true);
	
	if ($params["server"]!==true) { # True if linked to a server
		$GLOBALS['virtpanel_module_errors'][] = 'The product/service is not linked to a server inside WHMCS';
		return false;
	}
	$serverid = $params["serverid"];
	$serverusername = $params["serverusername"];
	$serverpassword = $params["serverpassword"];
	$serveraccesshash = $params["serveraccesshash"];
	$serversecure = $params["serversecure"]; # If set, SSL Mode is enabled in the server config
	
	$url = 'http'.($params['serversecure']=='on'?'s':'').'://'.$params['serverhostname'].'/'.$action;
	$vars['apikey'] = $params['serverpassword'];
	if(preg_match('/\?/', $action)) {
		$url .= '&';
	} else {
		$url .= '?';
	}
	foreach ($vars as $key => $value) {
		$url .= urlencode($key).'='.urlencode($value).'&';
	}
	
	$options = array(
		CURLOPT_URL				=> $url,
		CURLOPT_RETURNTRANSFER	=> true,
		CURLOPT_HEADER			=> false,
		CURLOPT_FOLLOWLOCATION	=> true,
		CURLOPT_USERAGENT		=> 'WHMCS',
		CURLOPT_AUTOREFERER		=> true,
		CURLOPT_CONNECTTIMEOUT	=> 10,
		CURLOPT_TIMEOUT			=> 300,
		CURLOPT_MAXREDIRS		=> 10,
		CURLOPT_SSL_VERIFYHOST	=> false,
		CURLOPT_SSL_VERIFYPEER	=> false,
	);
	$ch = curl_init();
	curl_setopt_array($ch, $options);
	$data = curl_exec($ch);
	if ($data===false) {
		$GLOBALS['virtpanel_module_errors'][] = 'Curl error: '.curl_error($ch);
		return false;
	}
	$data = trim($data);
	
	/*
	 *	find errors
	 */
	preg_match_all('/<error>(.*?)<\/error>/i', $data, $errors);
	$errors = $errors[1];
	if(count($errors)==0) {
		return $data;
	}
	
	foreach($errors as $error) {
		$GLOBALS['virtpanel_module_errors'][] = $error;
	}
	return false;
}

function virtpanel_return_errors() {
	$result = '';
	foreach($GLOBALS['virtpanel_module_errors'] as $error) {
		$result .= $error.' & ';
	}
	$result = substr($result, 0, -3);
	if (empty($result)) {
		$result = 'An unknown error has occured';
	}
	return $result;
}

function virtpanel_get_ip($params) {
	$data_ip = virtpanel_curl($params, 'api/get_ip.php', array(
		'username' => $params['username'],
	));
	$ips = explode(PHP_EOL, $data_ip);
	$ip_list = '';
	foreach($ips as $ip) {
		if (!filter_var($ip, FILTER_VALIDATE_IP)) {
			return 'Failed to get IP Addresses';
		}
		$ip_list .= $ip.', ';
	}
	$ip_list = substr($ip_list, 0, -2);
	
	// Update the IPs in database
	try {
		$updatedUserCount = Capsule::table('tblhosting')
			->where('id', $params['serviceid'])
			->update(
				[
					'assignedips' => str_replace(', ', PHP_EOL, $ip_list),
					'dedicatedip' => $ip_list,
				]
			);
	} catch (\Exception $e) {
		return "Failed to update service username in the database. {$e->getMessage()}";
	}
	
	return 'success';
}

function virtpanel_get_vm($params) {
	$json = virtpanel_curl($params, 'api/get_vm.php', array(
		'username' => $params['username'],
	));
	$array = json_decode($json, true);
	return $array;
}

function virtpanel_reboot($params) {
	$data = virtpanel_curl($params, '?vm', array(
		'action' => 'view',
		'vm' => $params['username'],
		'subaction' => 'reboot',
	));
	if ($data===false) {
		return virtpanel_return_errors();
	}
	if (stripos($data, 'successfully updated')!==false) {
		return "success";
	}
	return 'Unknown Error';
}

function virtpanel_user_login($params) {
	$result = virtpanel_ServiceSingleSignOn($params);
	if ($result['success']===true) {
		echo '<meta http-equiv="refresh" content="0;url='.$result['redirectTo'].'">';
		return 'success';
	}
	return $result['errorMsg'];
}