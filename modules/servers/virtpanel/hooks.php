<?php
/**
 * VirtPanel WHMCS Module v3
 *
 * @see https://www.virtpanel.com/User/Documentation/View/19
 *
 * @copyright Copyright (c) VirtPanel 2016
 * @license https://www.virtpanel.com/Licensing+Agreement VirtPanel Licensing Agreement
 */

/**
 * Insert a service item to the client area navigation bar.
 *
 * Demonstrates adding an additional link to the Services navbar menu that
 * provides a shortcut to a filtered products/services list showing only the
 * products/services assigned to the module.
 *
 * @param \WHMCS\View\Menu\Item $menu
 */
add_hook('ClientAreaPrimaryNavbar', 1, function ($menu)
{
    // Check whether the services menu exists.
    if (!is_null($menu->getChild('Services'))) {
        // Add a link to the module filter.
        $menu->getChild('Services')
            ->addChild(
                'My VPS Services',
                array(
                    'uri' => 'clientarea.php?action=services&module=virtpanel',
                    'order' => 15,
                )
            );
    }
});
